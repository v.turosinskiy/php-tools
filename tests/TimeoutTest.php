<?php

declare(strict_types=1);

namespace Tests;

use Mockery;
use Mockery\MockInterface;
use PHP\Tools\Contract\Time\TimeInterface;
use PHP\Tools\Contract\Timeout\TimeoutInterface;
use PHP\Tools\Timeout\Timeout;
use PHPUnit\Framework\TestCase;

final class TimeoutTest extends TestCase
{
    /**
     * @var TimeInterface|MockInterface
     */
    public TimeInterface $timeMock;

    public function testInfinity(): void
    {
        $timeout = 0;
        $service = $this->createService($timeout);

        $this->mockHrTime(0);
        $this->assertTrue($service->isTimeOver());
    }

    public function testTimeout(): void
    {
        $timeout = 2;
        $service = $this->createService($timeout);

        $this->mockHrTime($timeout - 1);
        $this->assertFalse($service->isTimeOver());

        $this->mockHrTime($timeout);
        $this->assertTrue($service->isTimeOver());
    }

    private function createService(int $timeoutInNanoSeconds = 0): TimeoutInterface
    {
        $this->timeMock = Mockery::mock(TimeInterface::class);

        $this->mockHrTime(0);
        $service = new Timeout(
            $this->timeMock,
            $timeoutInNanoSeconds
        );

        $this->assertInstanceOf(TimeoutInterface::class, $service);

        return $service;
    }

    private function mockHrTime(int $expected): void
    {
        $this->timeMock
            ->shouldReceive('hrTime')
            ->once()
            ->andReturn($expected)
        ;
    }
}
