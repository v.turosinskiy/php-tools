<?php

declare(strict_types=1);

namespace Tests;

use function abs;
use function hrtime;
use PHP\Tools\Contract\Time\TimeInterface;
use PHP\Tools\Time\Time;
use PHPUnit\Framework\TestCase;
use function time;

final class TimeTest extends TestCase
{
    public function testNow(): void
    {
        $time = $this->createService();

        $this->assertTrue(abs(time() - $time->time()) <= 1);
    }

    private function createService(): TimeInterface
    {
        $service = new Time();
        $this->assertInstanceOf(TimeInterface::class, $service);

        return $service;
    }

    public function testHrNow(): void
    {
        $time = $this->createService();

        $this->assertTrue(abs(hrtime(true) - $time->hrTime()) <= 100_000);
    }

    public function testUsleep(): void
    {
        $this->createService()->usleep(1);
    }
}
