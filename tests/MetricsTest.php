<?php

declare(strict_types=1);

namespace Tests;

use PHP\Tools\Contract\Metrics\MetricsInterface;
use PHP\Tools\Metrics\Metrics;
use PHPUnit\Framework\TestCase;

final class MetricsTest extends TestCase
{
    /**
     * Checks methods for collection metrics.
     */
    public function testMetrics(): void
    {
        /** @var MetricsInterface $service */
        $service = new Metrics();

        $this->assertEmpty($service->getMetrics());

        $record = $service->record('metric1_name', 'category_name');
        $service->increment('metric2_name');
        $record->stop();

        $metrics = $service->getMetrics();

        $this->assertArrayHasKey('methods:category_name:time', $metrics);
        $this->assertArrayHasKey('methods:category_name:metric1_name:time', $metrics);
        $this->assertEquals(1, $metrics['methods:category_name:calls']);
        $this->assertEquals(1, $metrics['methods:category_name:metric1_name:calls']);
        $this->assertEquals(1, $metrics['parameter:metric2_name']);

        $service->increment('metric2_name');

        $metrics = $service->getMetrics();

        $this->assertEquals(2, $metrics['parameter:metric2_name']);
    }

    /**
     * Checks that extended time metrics are not collected by default.
     */
    public function testDefaultTimeMeasures(): void
    {
        /** @var MetricsInterface $service */
        $service = new Metrics();

        $this->assertEmpty($service->getMetrics());

        $record = $service->record('category_name', 'metric1_name');
        $record->stop();

        $metrics = $service->getMetrics();

        $this->assertArrayNotHasKey('methods:category_name:metric1_name:avg_time', $metrics);
        $this->assertArrayNotHasKey('methods:category_name:metric1_name:min_time', $metrics);
        $this->assertArrayNotHasKey('methods:category_name:metric1_name:max_time', $metrics);
    }

    /**
     * Checks collection of extended time metrics.
     */
    public function testExtraTimeMeasures(): void
    {
        /** @var MetricsInterface $service */
        $service = new Metrics(true);

        $this->assertEmpty($service->getMetrics());

        for ($i = 1; $i < 3; ++$i) {
            $record = $service->record('metric1_name', 'category_name');
            usleep(500 * $i);
            $record->stop();
        }

        $metrics = $service->getMetrics();

        $this->assertArrayNotHasKey('methods:category_name:avg_time', $metrics);
        $this->assertArrayNotHasKey('methods:category_name:min_time', $metrics);
        $this->assertArrayNotHasKey('methods:category_name:max_time', $metrics);
        $this->assertArrayHasKey('methods:category_name:metric1_name:avg_time', $metrics);
        $this->assertArrayHasKey('methods:category_name:metric1_name:min_time', $metrics);
        $this->assertArrayHasKey('methods:category_name:metric1_name:max_time', $metrics);

        $min   = $metrics['methods:category_name:metric1_name:min_time'];
        $max   = $metrics['methods:category_name:metric1_name:max_time'];
        $avg   = $metrics['methods:category_name:metric1_name:avg_time'];
        $total = $metrics['methods:category_name:metric1_name:time'];

        $this->assertLessThan($avg, $min);
        $this->assertLessThan($max, $avg);
        $this->assertLessThan($total, $max);
    }

    public function testSetLabel(): void
    {
        $metrics = new Metrics();
        $metrics
            ->setLabel('some_string', 'some_value')
            ->setLabel('some_int', 1)
            ->setLabel('some_null', null)
        ;
        $this->assertEquals([
            'some_string' => 'some_value',
            'some_int'    => 1,
            'some_null'   => null,
        ], $metrics->getMetrics());
    }

    public function testSetLabelMultipleTimes(): void
    {
        $metrics = new Metrics();
        $metrics
            ->setLabel('key', 1)
            ->setLabel('key', 2)
        ;

        $this->assertEquals(['key' => 1], $metrics->getMetrics(), 'Only last value should be saved');
    }
}
