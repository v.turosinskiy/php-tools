<?php

declare(strict_types=1);

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__ . '/src')
    ->in(__DIR__ . '/tests')
;

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules(
        [
            '@PhpCsFixer' => true,
            '@PSR2' => true,
            '@Symfony' => true,
            'array_syntax' => ['syntax' => 'short'],
            'linebreak_after_opening_tag' => true,
            'no_superfluous_phpdoc_tags' => true,
            'no_useless_else' => true,
            'no_useless_return' => true,
            'ordered_imports' => true,
            'phpdoc_order' => true,
            'semicolon_after_instruction' => true,
            'global_namespace_import' => true,
            'constant_case' => ['case' => 'lower'],
            'concat_space' => ['spacing' => 'one'],
            'class_attributes_separation' => true,
            'class_definition' => [
                'single_line' => false,
                'multi_line_extends_each_single_line' => true,
                'single_item_single_line' => false,
            ],
            'php_unit_test_class_requires_covers' => false,
            'ordered_class_elements' => false,
            'array_indentation' => true,
            'php_unit_internal_class' => false,
            'declare_strict_types' => true,
            'single_blank_line_at_eof' => true,
            'binary_operator_spaces' => ['operators' => ['=>' => 'align', '=' => 'align']],
            'phpdoc_annotation_without_dot' => false
        ]
    )
    ->setFinder($finder)
;
