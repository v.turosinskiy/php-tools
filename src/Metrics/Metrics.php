<?php

declare(strict_types=1);

namespace PHP\Tools\Metrics;

use function max;
use function min;
use PHP\Tools\Contract\Metrics\MetricsInterface;
use PHP\Tools\Contract\Metrics\RecordInterface;
use PHP\Tools\Contract\Time\TimeInterface;
use PHP\Tools\Time\Time;

class Metrics implements MetricsInterface
{
    protected const METHODS_TAG = 'methods';

    protected const PARAMETER_TAG = 'parameter';

    protected const CALLS_TAG = 'calls';

    protected const TIME_TAG = 'time';

    protected const TIME_AVG_TAG = 'avg_time';

    protected const TIME_MIN_TAG = 'min_time';

    protected const TIME_MAX_TAG = 'max_time';

    private const TIMER_INDEX_START = 0;

    private const TIMER_INDEX_END = 1;

    private int $counter;

    private array $timers;

    private array $values;

    private bool $addExtraTimeMeasures;

    private array $labels = [];

    private TimeInterface $time;

    /**
     * @param bool $addExtraTimeMeasures Нужно ли добавлять минимальное/среднее/максимальное время выполнения.
     */
    public function __construct(
        bool $addExtraTimeMeasures = false,
        ?TimeInterface $time = null
    ) {
        $this->time                 = $time ?? new Time();
        $this->addExtraTimeMeasures = $addExtraTimeMeasures;

        $this->init();
    }

    /**
     * {@inheritDoc}
     */
    public function record(string $name, string $category): RecordInterface
    {
        $recordId                                   = $this->getNewRecordId();
        $this->timers[$category][$name][$recordId]  = [self::TIMER_INDEX_START => $this->time->hrTime()];

        return new class($category, $name, $recordId, $this) implements RecordInterface {
            private string $category;

            private string $name;

            private int $index;

            private Metrics $metrics;

            public function __construct(string $category, string $name, int $index, Metrics $metrics)
            {
                $this->category  = $category;
                $this->name      = $name;
                $this->index     = $index;
                $this->metrics   = $metrics;
            }

            /**
             * {@inheritDoc}
             */
            public function stop(): void
            {
                $this->metrics->stopRecord($this->category, $this->name, $this->index);
            }
        };
    }

    /**
     * {@inheritDoc}
     *
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public function increment(string $name, int $value = 1): MetricsInterface
    {
        if (array_key_exists($name, $this->values)) {
            $this->values[$name] += $value;
        } else {
            $this->values[$name] = $value;
        }

        return $this;
    }

    public function setLabel(string $name, $value): self
    {
        if (!array_key_exists($name, $this->labels)) {
            $this->labels[$name] = $value;
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getMetrics(): array
    {
        $result = [];

        $now = $this->time->hrTime();

        // Сначала метрики по таймерам
        foreach ($this->timers as $category => $methodsData) {
            $categoryCalls = 0;
            $categoryTime  = 0;
            foreach ($methodsData as $name => $startEndData) {
                $callsInfo = $this->createCallsInfo();
                foreach ($startEndData as $startEndPair) {
                    $start = $startEndPair[self::TIMER_INDEX_START];
                    $end   = $startEndPair[self::TIMER_INDEX_END] ?? $now;
                    $callsInfo->addTime($end - $start);
                }

                $categoryCalls += $callsInfo->getCalls();
                $categoryTime += $callsInfo->getTotalTime();

                $result[self::METHODS_TAG . ':' . $category . ':' . $name . ':' . self::CALLS_TAG] = $callsInfo->getCalls();
                $result[self::METHODS_TAG . ':' . $category . ':' . $name . ':' . self::TIME_TAG]  = $callsInfo->getTotalTime();

                if ($this->addExtraTimeMeasures) {
                    $result[self::METHODS_TAG . ':' . $category . ':' . $name . ':' . self::TIME_MIN_TAG] = $callsInfo->getMinTime();
                    $result[self::METHODS_TAG . ':' . $category . ':' . $name . ':' . self::TIME_MAX_TAG] = $callsInfo->getMaxTime();
                    $result[self::METHODS_TAG . ':' . $category . ':' . $name . ':' . self::TIME_AVG_TAG] = $callsInfo->getAvgTime();
                }
            }
            $result[self::METHODS_TAG . ':' . $category . ':' . self::CALLS_TAG] = $categoryCalls;
            $result[self::METHODS_TAG . ':' . $category . ':' . self::TIME_TAG]  = $categoryTime;
        }

        // Add counters
        foreach ($this->values as $name => $value) {
            $key          = self::PARAMETER_TAG . ':' . $name;
            $result[$key] = $value;
        }

        // Add labels
        foreach ($this->labels as $name => $value) {
            $result[$name] = $value;
        }

        return $result;
    }

    public function stopRecord(string $category, string $name, int $index): void
    {
        $this->timers[$category][$name][$index][self::TIMER_INDEX_END] = $this->time->hrTime();
    }

    private function getNewRecordId(): int
    {
        $value = $this->counter;
        ++$this->counter;

        return $value;
    }

    private function createCallsInfo(): object
    {
        return new class() {
            private int $calls = 0;

            private float $totalTime = 0;

            private float $minTime = 0;

            private float $maxTime = 0;

            public function addTime(float $time): void
            {
                ++$this->calls;
                $this->totalTime += $time;
                $this->minTime   = min($this->minTime, $time);
                $this->maxTime   = max($this->maxTime, $time);
            }

            public function getAvgTime(): float
            {
                return 0 === $this->calls ? 0 : $this->totalTime / $this->calls;
            }

            public function getMinTime(): float
            {
                return $this->minTime;
            }

            public function getMaxTime(): float
            {
                return $this->maxTime;
            }

            public function getTotalTime(): float
            {
                return $this->totalTime;
            }

            public function getCalls(): int
            {
                return $this->calls;
            }
        };
    }

    protected function init(): void
    {
        $this->counter = 0;
        $this->timers  = [];
        $this->values  = [];
        $this->labels  = [];
    }
}
