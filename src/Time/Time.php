<?php

declare(strict_types=1);

namespace PHP\Tools\Time;

use function hrtime;
use PHP\Tools\Contract\Time\TimeInterface;
use function time;
use function usleep;

class Time implements TimeInterface
{
    /**
     * {@inheritDoc}
     */
    public function time(): int
    {
        return time();
    }

    /**
     * {@inheritDoc}
     */
    public function hrTime(): int
    {
        return hrtime(true);
    }

    /**
     * {@inheritDoc}
     */
    public function usleep(int $microseconds): void
    {
        usleep($microseconds);
    }
}
