<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use PHP\Tools\Contract\Argument\ArgumentInterface;

class Argument implements ArgumentInterface
{
    public function __construct(
        private mixed $value
    ) {
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
