<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use PHP\Tools\Contract\Argument\IntArgumentInterface;

class IntArgument implements IntArgumentInterface
{
    public function __construct(
        private ?int $value
    ) {
    }

    public function getValue(): ?int
    {
        return $this->value;
    }
}
