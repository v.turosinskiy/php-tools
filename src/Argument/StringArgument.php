<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use PHP\Tools\Contract\Argument\StringArgumentInterface;

class StringArgument implements StringArgumentInterface
{
    public function __construct(
        private ?string $value
    ) {
    }

    public function getValue(): ?string
    {
        return $this->value;
    }
}
