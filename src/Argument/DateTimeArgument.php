<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use DateTime;
use PHP\Tools\Contract\Argument\DateTimeArgumentInterface;

class DateTimeArgument implements DateTimeArgumentInterface
{
    public function __construct(
        private ?DateTime $value
    ) {
    }

    public function getValue(): ?DateTime
    {
        return $this->value;
    }
}
