<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use PHP\Tools\Contract\Argument\BoolArgumentInterface;

class BoolArgument implements BoolArgumentInterface
{
    public function __construct(
        private ?bool $value
    ) {
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }
}
