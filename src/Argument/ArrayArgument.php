<?php

declare(strict_types=1);

namespace PHP\Tools\Argument;

use PHP\Tools\Contract\Argument\ArrayArgumentInterface;

class ArrayArgument implements ArrayArgumentInterface
{
    public function __construct(
        private ?array $value,
    ) {
    }

    public function getValue(): array
    {
        return $this->value ?? [];
    }
}
