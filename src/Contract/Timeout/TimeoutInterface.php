<?php

declare(strict_types=1);

namespace PHP\Tools\Contract\Timeout;

interface TimeoutInterface
{
    /**
     * Returns true if the timer has exceeded the specified timeout.
     */
    public function isTimeOver(): bool;
}
