<?php

declare(strict_types=1);

namespace PHP\Tools\Contract\Time;

interface TimeInterface
{
    public const SECONDS_TO_NANOSECONDS_RATIO = 1_000_000_000;

    public const SECONDS_TO_MICROSECONDS_RATIO = 1_000_000;

    /**
     * Return time in seconds.
     */
    public function time(): int;

    /**
     * Return time in nanoseconds.
     */
    public function hrTime(): int;

    /**
     * Call sleep in microseconds.
     */
    public function usleep(int $microseconds): void;
}
