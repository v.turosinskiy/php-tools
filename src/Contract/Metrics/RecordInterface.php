<?php

declare(strict_types=1);

namespace PHP\Tools\Contract\Metrics;

interface RecordInterface
{
    /**
     * Stops the timer.
     */
    public function stop(): void;
}
