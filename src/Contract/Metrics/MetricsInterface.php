<?php

declare(strict_types=1);

namespace PHP\Tools\Contract\Metrics;

interface MetricsInterface
{
    /**
     * Starts a timer for a value.
     *
     * @param string $name     Value name.
     * @param string $category The category to which the value name belongs.
     */
    public function record(string $name, string $category): RecordInterface;

    /**
     * Increase value.
     *
     * @param string $name  Value name.
     * @param int    $value How much to increase value.
     */
    public function increment(string $name, int $value = 1): self;

    /**
     * @param string $name  Label name
     * @param mixed  $value Label value
     *
     * @return $this
     */
    public function setLabel(string $name, mixed $value): self;

    /**
     * Returns the current values of timers and values.
     */
    public function getMetrics(): array;
}
