<?php

declare(strict_types=1);

namespace PHP\Tools\Timeout;

use PHP\Tools\Contract\Time\TimeInterface;
use PHP\Tools\Contract\Timeout\TimeoutInterface;

class Timeout implements TimeoutInterface
{
    private TimeInterface $time;

    private int $timeoutInNanoSeconds;

    private int $nextTimestamp;

    public function __construct(
        TimeInterface $time,
        int $timeoutInNanoSeconds
    ) {
        $this->time                 = $time;
        $this->timeoutInNanoSeconds = $timeoutInNanoSeconds;

        $this->moveTimeToNext();
    }

    public function isTimeOver(): bool
    {
        $result = $this->nextTimestamp <= $this->time->hrTime();

        if ($result) {
            $this->moveTimeToNext();
        }

        return $result;
    }

    private function moveTimeToNext(): void
    {
        $this->nextTimestamp = $this->time->hrTime() + $this->timeoutInNanoSeconds;
    }
}
