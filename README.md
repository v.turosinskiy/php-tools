## Description

The project contains useful tools that can be reused between projects.

- `Metrics` allows you to collect various metrics of the application (the number of calls, call time, various counters, etc.). Easily integrates with ELK.
- `Time` allows you to mock global time functions.
- `Timeout` allows you to create a timer to perform operations once in a specified period.

## Install

```bash
composer require v.turosinskiy/php-tools:^1.0
```

## Usage

### Timeout
```php
use \PHP\Tools\Timeout\Timeout;
use Psr\Log\LoggerInterface;

$timeoutInSeconds = 3;
$timeoutInNanoseconds = 3 * \PHP\Tools\Time\Time::SECONDS_TO_NANOSECONDS_RATIO;
$service = new Timeout($timeoutInNanoseconds);

while (true){
    //do some work
    if($service->isTimeOver()){//return true every 3 seconds
        //log results or do another work
    }
}
```

### Metrics

```php
use \PHP\Tools\Metrics\Metrics;

$service = new Metrics();

//add countable metrics
$service->increment('my_value'); # increase value by 1
$service->increment('my_value', 100); # increase value by 100

//add time metrics
$record = $service->record('my_query_name', 'db'); # start timer

//do request to db for example
$record->stop();// stop timer

//add label
$service->setLabel('my_label', 'my_value');

$metrics = $service->getMetrics();// Get metrics
```

### Time 

```php
use \PHP\Tools\Time\Time;

$service = new Time();

$hrTime = $service->hrTime();
$time = $service->time();

$timeoutInSeconds = 3 * Time::SECONDS_TO_MICROSECONDS_RATIO;
$hrTime = $service->usleep($timeoutInSeconds);
```

## Tests

Run unit tests:
```bash
composer test-unit
```

Check coverage:
```bash
composer coverage
```

Fix code style:
```bash
composer cs-fix
```

Check with phpstan:
```bash
composer phpstan
```
